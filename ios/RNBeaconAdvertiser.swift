//
//  RNBeaconAdvertiser.swift
//  RNBleAdvertiser
//
//  Created by Kovacs Nandor on 2019. 01. 27..
//  Copyright © 2019. Facebook. All rights reserved.
//

//import Foundation

import Foundation
import CoreLocation
import CoreBluetooth

@objc(RNBeaconAdvertiser)
class RNBeaconAdvertiser: NSObject, CBPeripheralManagerDelegate {
  
  var myBeacon: CLBeaconRegion!
  var beaconData: NSDictionary!
  var peripheralMgr: CBPeripheralManager!
  
  let myRegionID = "ABRegionID"
  
  @objc
  func startAdvertising(_ message: String, major: NSNumber, minor: NSNumber) -> Void {
    print("Message ", message)
    print("Major ", major)
    print("Minor ", minor)
    
    // don't do anything if myBeacon already exists
    guard myBeacon == nil else { return }
    
    let beaconUUID = "7FA08BC7-A55F-45FC-85C0-0BF26F899530"
    let beaconMajor: CLBeaconMajorValue = UInt16(major)
    let beaconMinor: CLBeaconMinorValue = UInt16(minor)
    
    let uuid = UUID(uuidString: beaconUUID)!
    myBeacon = CLBeaconRegion(proximityUUID: uuid, major: beaconMajor, minor: beaconMinor, identifier: "MyIdentifier")
    
    beaconData = myBeacon.peripheralData(withMeasuredPower: nil)
    peripheralMgr = CBPeripheralManager(delegate: self, queue: nil, options: nil)
  }
  
  @objc
  func stopAdvertising(_ message: String) -> Void {
    print("Message ", message)
    // don't do anything if there's no peripheral manager
    guard peripheralMgr != nil else { return }
    
    peripheralMgr.stopAdvertising()
    peripheralMgr = nil
    beaconData = nil
    myBeacon = nil
  }
  
  @objc
  func constantsToExport() -> [String: Any]! {
    return ["someKey": "someValue"]
  }
  
  func peripheralManagerDidUpdateState(_ peripheral: CBPeripheralManager) {
    if peripheral.state == .poweredOn {
      peripheralMgr.startAdvertising(beaconData as! [String: AnyObject]!)
    } else if peripheral.state == .poweredOff {
      peripheralMgr.stopAdvertising()
    }
  }
  
}
