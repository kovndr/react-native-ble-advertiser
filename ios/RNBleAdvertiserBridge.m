//
//  RNBleAdvertiserBridge.m
//  RNBleAdvertiser
//
//  Created by Kovacs Nandor on 2019. 01. 27..
//  Copyright © 2019. Facebook. All rights reserved.
//

//#import <Foundation/Foundation.h>

#import <React/RCTBridgeModule.h>

@interface RCT_EXTERN_MODULE(RNBeaconAdvertiser, NSObject)

RCT_EXTERN_METHOD(startAdvertising:(NSString *)message major:(NSNumber *)major minor:(NSNumber *)minor)
RCT_EXTERN_METHOD(stopAdvertising:(NSString *)message)

@end
